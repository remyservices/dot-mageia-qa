#!/bin/bash
# Last updated 1/14/2016

if [ "$MGA_QA_CUPDT" == "" ]; then
  export MGA_QA_CUPDT=0
  export MGA_QA_CBUPDT=0
  export MGA_QA_NFUPDT=0
  export MGA_QA_NFBUPDT=0
  export MGA_QA_TUPDT=0
  export MGA_QA_TBUPDT=0
fi

function qa-testing-status {
  case $1 in
    "" )
      echo Core Updates Testing:      $MGA_QA_CUPDT
      echo Core Backports Testing:    $MGA_QA_CBUPDT
      echo Nonfree Updates Testing:   $MGA_QA_NFUPDT
      echo Nonfree Backports Testing: $MGA_QA_NFBUPDT
      echo Tainted Updates Testing:   $MGA_QA_TUPDT
      echo Tainted Backports Testing: $MGA_QA_TBUPDT
      ;;

    "cupdt" )
      if [ "$2" == "1" ]; then
        export MGA_QA_CUPDT=1
      else
        export MGA_QA_CUPDT=0
      fi
      ;;

    "cbupdt" )
      if [ "$2" == "1" ]; then
        export MGA_QA_CBUPDT=1
      else
        export MGA_QA_CBUPDT=0
      fi
      ;;

    "nfupdt" )
      if [ "$2" == "1" ]; then
        export MGA_QA_NFUPDT=1
      else
        export MGA_QA_NFUPDT=0
      fi
      ;;

    "nfbupdt" )
      if [ "$2" == "1" ]; then
        export MGA_QA_NFBUPDT=1
      else
        export MGA_QA_NFBUPDT=0
      fi
      ;;

    "tupdt" )
      if [ "$2" == "1" ]; then
        export MGA_QA_TUPDT=1
      else
        export MGA_QA_TUPDT=0
      fi
      ;;

    "tbupdt" )
      if [ "$2" == "1" ]; then
        export MGA_QA_TBUPDT=1
      else
        export MGA_QA_TBUPDT=0
      fi
      ;;

    *)
      echo Unknown option...
      ;;
  esac
}

function qa-verify-logs {
  SUDO=''
  if [ $EUID -ne 0 ]; then
    SUDO='sudo'
  fi

  # check dmesg for 'error' and 'warning'
  "echo" -e "\e[1;32mdmesg errors and warnings\e[0m"
  $SUDO "dmesg" -t -l emerg,alert,crit,err,warn | "sort" | "uniq" -c
  "echo"

  # check new files in /var/log/ for 'error' and 'warning'
  "echo" -e "\e[1;32mParsing /var/log/ for errors and warnings\e[0m"
  sudo "grep" -r -i -I --exclude=\*\{\.\?,\.old,\.today,\.yesterday,msec\.log,security\.log\} -e warning -e error -e \(WW\) -e \(EE\) -e crit -e invalid /var/log/ | "sed" -e 's/\[.*\]//g' | "sed" -e "s/$HOSTNAME //g" | "sed" -e 's/[a-zA-Z]\{3\} [0-9]\{2\} [0-9:]\{8\} //g' | "sort" | "uniq" -c
  "echo"
}

function qa-report-template {
  SUDO=''
  if [ $EUID -ne 0 ]; then
  ︙ SUDO='sudo'
  fi

  "echo" "Tested on:"
  printf "  "%s "`cat /etc/release`"
  "echo"
  "echo"

  "echo" "Package(s) Under Test:"
  "echo" "  ** List each package by short name **"
  "echo"

  "echo" "Package(s) Testing Pre Upgrade:"
  "echo" "  ** Show output of each 'urpmi <package>' **"
  "echo" "  ** This should show that it is already installed **"
  "echo" "  ** Rerun if needed to show it is installed **"
  "echo" "  ** List notes confirming package is working and steps taken **"
  "echo"

  "echo" "Package(s) Testing Upgrade:"
  "echo" "  ** After testing repo is added, run urpmi <package> again to upgrade **"
  "echo" "  ** Then run urpmi <package> and list output to confirm upgrade **"
  "echo" "  ** List notes confirming package is working and steps taken **"
  "echo"

  "echo" "Kernal Version:"
  printf "  "%s "`uname -rp`"
  "echo"
  "echo"

  "echo" "Hardware Information:"
  $SUDO lshw -class system | 'grep' -e desc -e prod -e vend | sed 's/^    //g'
}
